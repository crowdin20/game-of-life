# README #

Game of Life by Anurag Kumar with Sphinx documentation

## What is this repository for? ##

Example of Sphinx documentation

## Make instructions ##


English:

    make html

Russian:

    make -e SPHINXOPTS="-D language='ru'" html

German:

    make -e SPHINXOPTS="-D language='de'" html
    
## Configuration ##

Install nginx and set root to **<project_dir>/docs/build/html**

## How to make changes to documentation ##

1. Edit .rst files or update with autodoc
2. `make gettext`
3. `sphinx-intl update -p build/gettext -l de -l ru`
4. Edit translation in source/locale/<de/ru>/LC_MESSAGES/*.po

## Dependencies ##

pip packages:

* numpy
* matplotlib
* sphinx-intl
* autodoc
    
system packages:

* python3-sphinx
