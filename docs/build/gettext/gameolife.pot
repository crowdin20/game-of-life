# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2020, Anurag Kumar
# This file is distributed under the same license as the Game of Life package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Game of Life \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-04-02 20:23+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../source/gameolife.rst:2
msgid "Game of Life"
msgstr ""

#: ../../source/gameolife.rst:5
msgid "Description"
msgstr ""

#: ../../source/gameolife.rst:6
msgid "Conway's Game Of Life, Author Anurag Kumar(mailto:anuragkumarak95@gmail.com)"
msgstr ""

#: ../../source/gameolife.rst:8
msgid "`Github <https://github.com/geekcomputers/Python/blob/master/game_of_life>`_"
msgstr ""

#: ../../source/gameolife.rst:11
msgid "Requirements"
msgstr ""

#: ../../source/gameolife.rst:12
msgid "numpy"
msgstr ""

#: ../../source/gameolife.rst:13
msgid "random"
msgstr ""

#: ../../source/gameolife.rst:14
msgid "time"
msgstr ""

#: ../../source/gameolife.rst:15
msgid "matplotlib"
msgstr ""

#: ../../source/gameolife.rst:18
msgid "Usage"
msgstr ""

#: ../../source/gameolife.rst:24
msgid "Game-Of-Life Rules"
msgstr ""

#: ../../source/gameolife.rst:25
msgid "Any live cell with fewer than two live neighbours dies, as if caused by under-population."
msgstr ""

#: ../../source/gameolife.rst:26
msgid "Any live cell with two or three live neighbours lives on to the next generation."
msgstr ""

#: ../../source/gameolife.rst:27
msgid "Any live cell with more than three live neighbours dies, as if by over-population."
msgstr ""

#: ../../source/gameolife.rst:28
msgid "Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction."
msgstr ""

#: ../../source/gameolife.rst:31
msgid "Members"
msgstr ""

#: ../../../gameoflife/game_o_life.py:docstring of game_o_life.create_canvas:1
msgid "This function creates canvas for the game."
msgstr ""

#: ../../../gameoflife/game_o_life.py:docstring of game_o_life.create_canvas:0
#: ../../../gameoflife/game_o_life.py:docstring of game_o_life.run:0
#: ../../../gameoflife/game_o_life.py:docstring of game_o_life.seed:0
msgid "Parameters"
msgstr ""

#: ../../../gameoflife/game_o_life.py:docstring of game_o_life.create_canvas:3
msgid "size of canvas"
msgstr ""

#: ../../../gameoflife/game_o_life.py:docstring of game_o_life.create_canvas:0
#: ../../../gameoflife/game_o_life.py:docstring of game_o_life.run:0
msgid "Returns"
msgstr ""

#: ../../../gameoflife/game_o_life.py:docstring of game_o_life.create_canvas:5
msgid "bool[][] -- array for canvas"
msgstr ""

#: ../../../gameoflife/game_o_life.py:docstring of game_o_life.run:1
msgid "This  function runs the rules of game through all points, and changes their status accordingly (in the same canvas)."
msgstr ""

#: ../../../gameoflife/game_o_life.py:docstring of game_o_life.run:3
msgid "Canvas of population to run the rules on."
msgstr ""

#: ../../../gameoflife/game_o_life.py:docstring of game_o_life.run:5
msgid "None"
msgstr ""

#: ../../../gameoflife/game_o_life.py:docstring of game_o_life.seed:1
msgid "This function fills canvas with random values."
msgstr ""

#: ../../../gameoflife/game_o_life.py:docstring of game_o_life.seed:3
msgid "array for canvas to fill"
msgstr ""
