.. Game of Life documentation master file, created by
   sphinx-quickstart on Sat Feb 29 18:07:33 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Game of Life's documentation! (changed)
==================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   gameolife.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
